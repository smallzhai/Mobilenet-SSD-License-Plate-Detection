from hyperlpr import *
import cv2 as cv
import os

#
# opencv-python==4.X  no match
#
# python3  -m pip install  opencv-python==3.4.9.33
# python3  -m pip install hyperlpr
frameNum = 0
cap = cv.VideoCapture(0)  # 打开相机
print("avi  start")
while (True):
    try:
        ret, frame = cap.read()
        # 如果正确读取帧，ret为True
        if not ret:
            print("Can't receive frame (stream end?). Exiting ...")
            break
        cv.imshow("resized", frame)  # 显示视频
        if cv.waitKey(1) & 0xFF == ord('q'):
            break
    except:
        print("Unexpected error:", sys.exc_info()[0])
cap.release()
cv.destroyAllWindows()
