from hyperlpr import *
import cv2 as cv
from PIL import Image, ImageFont, ImageDraw
import numpy as np
import os

#
# opencv-python==4.X  no match
#
# python3  -m pip install  opencv-python==3.4.9.33
# python3  -m pip install hyperlpr

cvNet = cv.dnn.readNetFromCaffe("/home/pi/Mobilenet-SSD-License-Plate-Detection/mssd512_voc.prototxt",
                                "/home/pi/Mobilenet-SSD-License-Plate-Detection/mssd512_voc.caffemodel")

font = ImageFont.truetype('/home/pi/Mobilenet-SSD-License-Plate-Detection/Font_platech.ttf', 20)
# cv.namedWindow("resized", 0)  # 0可以改变窗口大小了
# cv.resizeWindow("resized", 1024, 768);


def detect(im):
    to_draw = im.copy()
    pixel_means = [0.406, 0.456, 0.485]
    pixel_stds = [0.225, 0.224, 0.229]
    pixel_scale = 255.0
    rows, cols, c = im.shape
    im_tensor = np.zeros((1, 3, im.shape[0], im.shape[1]))
    im = im.astype(np.float32)
    for i in range(3):
        im_tensor[0, i, :, :] = (im[:, :, 2 - i] / pixel_scale - pixel_means[2 - i]) / pixel_stds[2 - i]
    cvNet.setInput(im_tensor)
    cvOut = cvNet.forward()
    for detection in cvOut[0, 0, :, :]:
        score = float(detection[2])
        if score > 0.8:
            left = int(detection[3] * cols)
            top = int(detection[4] * rows)
            right = int(detection[5] * cols)
            bottom = int(detection[6] * rows)
            cropped = to_draw[top:bottom, left:right]
            # 识别结果
            text = HyperLPR_plate_recognition(cropped)
            if len(text) > 0:
                cv.rectangle(to_draw, (left, top), (right, bottom), (0, 255, 0), 1)
                pil_image = Image.fromarray(cv.cvtColor(to_draw, cv.COLOR_BGR2RGB))
                color = (0, 255, 0)
                pos = (left, top - 50)
                draw = ImageDraw.Draw(pil_image)
                draw.text(pos, text[0][0], font=font, fill=color)
                cv_img = cv.cvtColor(np.asarray(pil_image), cv.COLOR_RGB2BGR)
                # cv.imshow("resized", cv_img)  # 显示视频
                cv.imshow("resized", frame)  # 显示视频
                cv.imwrite(
                    '/home/pi/video/vieo1_' + str(
                        frameNum) + ".jpg",
                    cv_img)  # 保存图片
                break


frameNum = 0
cap = cv.VideoCapture(0)  # 打开相机
print("avi  start")
while (True):
    try:
        ret, frame = cap.read()
        frameNum = frameNum + 1
        print(frameNum)
        # 如果正确读取帧，ret为True
        if not ret:
            print("Can't receive frame (stream end?). Exiting ...")
            break
        if frameNum % 5 == 0:  # 调整帧数
            detect(frame)
        else:
            cv.imshow("resized", frame)  # 显示视频
        if cv.waitKey(1) & 0xFF == ord('q'):
            break
    except:
        print("Unexpected error:", sys.exc_info()[0])
cap.release()
cv.destroyAllWindows()
