from hyperlpr import *
import cv2 as cv
from PIL import Image, ImageFont, ImageDraw
import numpy as np
import os

#
# opencv-python==4.X  no match
#
# python3  -m pip install  opencv-python==3.4.9.33
# python3  -m pip install hyperlpr

cvNet = cv.dnn.readNetFromCaffe("/Users/renzhong.zhai/codex/Mobilenet-SSD-License-Plate-Detection/mssd512_voc.prototxt",
                                "/Users/renzhong.zhai/codex/Mobilenet-SSD-License-Plate-Detection/mssd512_voc.caffemodel")


def detect(im):
    to_draw = im.copy()
    pixel_means = [0.406, 0.456, 0.485]
    pixel_stds = [0.225, 0.224, 0.229]
    pixel_scale = 255.0
    rows, cols, c = im.shape
    im_tensor = np.zeros((1, 3, im.shape[0], im.shape[1]))
    im = im.astype(np.float32)
    for i in range(3):
        im_tensor[0, i, :, :] = (im[:, :, 2 - i] / pixel_scale - pixel_means[2 - i]) / pixel_stds[2 - i]
    cvNet.setInput(im_tensor)
    # print(im_tensor.shape)
    import time
    # cvOut = cvNet.forward()
    print(range(1))
    for _ in range(1):
        t0 = time.time()

        cvOut = cvNet.forward()
        # print(time.time() -t0)
    for detection in cvOut[0, 0, :, :]:
        score = float(detection[2])
        if score > 0.6:
            left = int(detection[3] * cols)
            top = int(detection[4] * rows)
            right = int(detection[5] * cols)
            bottom = int(detection[6] * rows)
            cropped = to_draw[top:bottom, left:right]
            t1 = time.time()
            # 识别结果
            text = HyperLPR_plate_recognition(cropped)
            print(text[0])
            print(time.time() - t1)
            # cv.imshow("cropped" , cropped)
            cv.rectangle(to_draw, (left, top), (right, bottom), (0, 255, 0), 1)
            # text1 = text[0][0]
            # pos = (left, top - 10)
            # font_type = 4
            # font_size = 1.5
            # color = (0, 255, 0)
            # bold = 0
            # # 图片，文字，位置，字体，字号，颜色，厚度
            # cv.putText(to_draw, zh_ch(text1), pos, font_type, font_size, color, bold)

            pil_image = Image.fromarray(cv.cvtColor(to_draw, cv.COLOR_BGR2RGB))
            font = ImageFont.truetype('/Users/renzhong.zhai/Downloads/Font_platech.ttf', 20)
            color = (0, 255, 0)
            pos = (left, top - 50)
            draw = ImageDraw.Draw(pil_image)
            draw.text(pos, text[0][0], font=font, fill=color)
            cv_img = cv.cvtColor(np.asarray(pil_image), cv.COLOR_RGB2BGR)
            cv.imshow('www.linuxidc.com', cv_img)
            # cv.imshow('image1', to_draw)
    cv.waitKey(0)


folderk = "/Users/renzhong.zhai/codex/Mobilenet-SSD-License-Plate-Detection/img/"
for filename in os.listdir(folderk):
    path = os.path.join(folderk, filename)
    if filename.lower().endswith(".jpeg"):
        image = cv.imread(path)
        detect(image)
