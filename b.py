import cv2
from PIL import Image,ImageFont,ImageDraw
import numpy as np

img1 = cv2.imread('/Users/renzhong.zhai/codex/Mobilenet-SSD-License-Plate-Detection/images/2.png',cv2.IMREAD_COLOR)

pil_image = Image.fromarray(cv2.cvtColor(img1, cv2.COLOR_BGR2RGB))

font = ImageFont.truetype('/Users/renzhong.zhai/Downloads/Font_platech.ttf', 40)
color = (0,0,255)
pos = (10,150)
text = u"Linux公社www.linuxidc.com"

draw = ImageDraw.Draw(pil_image)
draw.text(pos,text,font=font,fill=color)

cv_img = cv2.cvtColor(np.asarray(pil_image),cv2.COLOR_RGB2BGR)

cv2.imshow('www.linuxidc.com',cv_img)

cv2.waitKey(0)