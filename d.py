import numpy as np
import cv2 as cv

frameNum = 0
cap = cv.VideoCapture('/Users/renzhong.zhai/Desktop/007.mp4')
while cap.isOpened():
    ret, frame = cap.read()
    frameNum = frameNum + 1
    # 如果正确读取帧，ret为True
    if not ret:
        print("Can't receive frame (stream end?). Exiting ...")
        break
    if frameNum % 2 == 0:  # 调整帧数
        cv.imwrite('/Users/renzhong.zhai/codex/Mobilenet-SSD-License-Plate-Detection/video/vieo1_' + str(frameNum) + ".jpg", frame)  # 保存图片
        cv.namedWindow("resized", 0)  # 0可以改变窗口大小了
        # cv2.resizeWindow("resized", 640, 480) # 设置固定大小不能该，上面和一起使用
        cv.imshow("resized", frame)  # 显示视频
        if cv.waitKey(1) & 0xFF == ord('q'):
            break
cap.release()
cv.destroyAllWindows()